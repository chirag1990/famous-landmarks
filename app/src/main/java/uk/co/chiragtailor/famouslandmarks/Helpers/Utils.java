package uk.co.chiragtailor.famouslandmarks.Helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import uk.co.chiragtailor.famouslandmarks.R;

public class Utils
{
	public static ProgressDialog dialog;
	public static AlertDialog alert;
	private static String TAG = Utils.class.getSimpleName();

	//Check the internet connection
	public static boolean isNetworkAvailable(final Context context, boolean canShowErrorDialogOnFail, final boolean isFinish)
	{
		boolean isNetAvailable = false;
		if (context != null)
		{
			final ConnectivityManager mConnectivityManager = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);

			if (mConnectivityManager != null)
			{
				boolean mobileNetwork = false;
				boolean wifiNetwork = false;
				boolean mobileNetworkConnecetd = false;
				boolean wifiNetworkConnecetd = false;

				final NetworkInfo mobileInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
				final NetworkInfo wifiInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

				if (mobileInfo != null) {
					mobileNetwork = mobileInfo.isAvailable();
				}

				if (wifiInfo != null) {
					wifiNetwork = wifiInfo.isAvailable();
				}

				if (wifiNetwork || mobileNetwork) {
					if (mobileInfo != null)
						mobileNetworkConnecetd = mobileInfo
								.isConnectedOrConnecting();
					wifiNetworkConnecetd = wifiInfo.isConnectedOrConnecting();
				}

				isNetAvailable = (mobileNetworkConnecetd || wifiNetworkConnecetd);
			}
			context.setTheme(R.style.AppTheme);
			if (!isNetAvailable && canShowErrorDialogOnFail) {
				Log.v("TAG", "context : " + context.toString());
				if (context instanceof Activity) {
					((Activity) context).runOnUiThread(new Runnable() {

						@Override
						public void run() {
							showAlertWithFinish((Activity) context, context.getString(R.string.app_name), context.getString(R.string.network_alert), isFinish);
						}
					});
				}
			}
		}

		return isNetAvailable;
	}
	//Show ProgressDialog Helper
	public static void ShowProgressDialog(Activity activity, String message){
		if(dialog != null){
			dialog.dismiss();
		}
		try{
			dialog = new ProgressDialog(activity);
			dialog.setMessage(message);
			dialog.setCancelable(false);
			dialog.show();
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	//Hide ProgressDialog Helper
	public static void hideProgressDialog(){
		if(dialog != null && dialog.isShowing())
			dialog.dismiss();
	}

	//Alert helper
	private static void showAlertWithFinish(final Activity activity, String title, String message, final boolean isFinish) {
		android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (isFinish) {
					dialog.dismiss();
					activity.finish();
				} else {
					dialog.dismiss();
				}
			}
		}).show();
	}



	//get path of resize image
	public static String getSmallImageResizeImagePath(Activity activity,String imageUri, String name)
	{
		Log.e("","Name:::: "+name);
		//Resize image
		String mPath = "";
		final File file = new File(imageUri);
		if (file.exists())
		{
			Bitmap myBitmap = Utils.compressImageSmall(activity, imageUri);
			Log.e("","size: "+myBitmap.getWidth()+ "-"+myBitmap.getHeight());
			if(myBitmap != null)
			{
				mPath = Environment.getExternalStorageDirectory().toString()+ "/"+name+".jpeg";
				OutputStream fout = null;
				final File imageFile = new File(mPath);

				try {
					fout = new FileOutputStream(imageFile);
					myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fout);
					fout.flush();
					fout.close();
				} catch (final FileNotFoundException e) {
					e.printStackTrace();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
		}

		return mPath;
	}

	//convert bigger image to smaller
	public static Bitmap compressImageSmall(Activity activity, String path)
	{
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
		Bitmap thumb = null;

		Drawable drawable = new BitmapDrawable(activity.getResources(), bitmap);
		int widthMeasureSpec = 200, heightMeasureSpec = 200, width, height;

		float imageSideRatio = (float)drawable.getIntrinsicWidth() / (float)drawable.getIntrinsicHeight();
		float viewSideRatio = (float) View.MeasureSpec.getSize(widthMeasureSpec) / (float) View.MeasureSpec.getSize(heightMeasureSpec);
		if (imageSideRatio >= viewSideRatio) {
			// Image is wider than the display (ratio)
			width = View.MeasureSpec.getSize(widthMeasureSpec);
			height = (int)(width / imageSideRatio);
			thumb = Bitmap.createScaledBitmap(bitmap, width, height, false);

			if(thumb.getHeight() < heightMeasureSpec)
			{
				height = View.MeasureSpec.getSize(heightMeasureSpec);
				width = (int)(height * imageSideRatio);
				thumb = Bitmap.createScaledBitmap(bitmap, width, height, false);
			}
		} else {
			// Image is taller than the display (ratio)
			height = View.MeasureSpec.getSize(heightMeasureSpec);
			width = (int)(height * imageSideRatio);
			thumb = Bitmap.createScaledBitmap(bitmap, width, height, false);

			if(thumb.getWidth() < widthMeasureSpec)
			{
				width = View.MeasureSpec.getSize(widthMeasureSpec);
				height = (int)(width / imageSideRatio);
				thumb = Bitmap.createScaledBitmap(bitmap, width, height, false);
			}
		}
		return thumb;
	}
}
