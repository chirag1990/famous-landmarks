package uk.co.chiragtailor.famouslandmarks;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import uk.co.chiragtailor.famouslandmarks.Helpers.AlertDialogBuilder;
import uk.co.chiragtailor.famouslandmarks.Helpers.DatabaseHelper;
import uk.co.chiragtailor.famouslandmarks.Models.LocationModel;

public class LocationDetails extends AppCompatActivity {
    //initialising variables
    private LocationModel locationModel;
    private ProgressBar progressBar;
    private DatabaseHelper dbHelper;
    public static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_details);
        // initialising databaseHelper
        dbHelper = new DatabaseHelper(LocationDetails.this);
        activity = LocationDetails.this;

        //getting action bar (menu) and seeing the home/back button
        try {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

        //initialising fields
        ImageView locationImage = (ImageView) findViewById(R.id.imageViewOnDetails);
        TextView locationDesc = (TextView) findViewById(R.id.textViewDesc);
        Button btnDelete = (Button) findViewById(R.id.btnDelete);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        //initialising bundle and check if the bundle is not null
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //getting the locationModel and storing it in json
            String json = bundle.getString("locationModel");
            //setting up Location Model class with locationModel data
            locationModel = new Gson().fromJson(json, LocationModel.class);
            //Code to check user upload data for images
            if (locationModel.getUserUpload()) {
                progressBar.setVisibility(View.GONE);
                btnDelete.setVisibility(View.VISIBLE);
                byte[] decodedString = Base64.decode(locationModel.getImage(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                locationImage.setImageBitmap(decodedByte);
            } else {
                btnDelete.setVisibility(View.GONE);
                ImageLoader.getInstance().displayImage(locationModel.getImage(), locationImage, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }

            //setting description text
            locationDesc.setText(locationModel.getLongDesc());
        }

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Delete landmark data from database
                dbHelper.delete(Integer.parseInt(locationModel.get_id()));
                Toast.makeText(LocationDetails.this, "Data deleted successfully", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(LocationDetails.this, ListActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //initialising MenuInflater
        MenuInflater inflater = getMenuInflater();
        //calling the MenuInflater's inflate function, we pass the main_menu.xml
        inflater.inflate(R.menu.main_menu, menu);
        //removing extra menu option as its not need on this activity screen.
        menu.removeItem(R.id.refreshList);
        menu.removeItem(R.id.addLocation);
        menu.removeItem(R.id.saveLocation);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //switching on which button was clicked
        switch (item.getItemId()) {
            //case home is for back button so the user can go back to main screen
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.internetDetails:
                //starting a new activity to webView and passing locationModel
                Intent intent = new Intent(LocationDetails.this, webView.class);
                intent.putExtra("locationModel", new Gson().toJson(locationModel));
                startActivity(intent);
                break;
            case R.id.mapDetails:
                //starting a new activity to mapView and passing locationModel
                Intent mapIntent = new Intent(LocationDetails.this, MapsActivity.class);
                mapIntent.putExtra("locationModel", new Gson().toJson(locationModel));
                startActivity(mapIntent);
                break;
            //next two cases calls alert dialog box
            case R.id.help:
                AlertDialogBuilder.alertDialogShow(LocationDetails.this, "help");
                break;
            case R.id.about:
                AlertDialogBuilder.alertDialogShow(LocationDetails.this, "about");
                break;
            case R.id.editLocation:
                //check the value of userUpload
                if (locationModel.getUserUpload()) {
                    //if the user has uploaded the data start a new activity editLocation and pass locationModel
                    Intent editIntent = new Intent(LocationDetails.this, EditLocation.class);
                    editIntent.putExtra("locationModel", new Gson().toJson(locationModel));
                    startActivity(editIntent);
                } else {
                    //else let the user know they cannot edit the current data
                    Toast.makeText(LocationDetails.this, "You can not edit this data", Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
