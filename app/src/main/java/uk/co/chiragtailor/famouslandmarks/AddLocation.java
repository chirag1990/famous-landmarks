package uk.co.chiragtailor.famouslandmarks;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import uk.co.chiragtailor.famouslandmarks.Helpers.AlertDialogBuilder;
import uk.co.chiragtailor.famouslandmarks.Helpers.DatabaseHelper;
import uk.co.chiragtailor.famouslandmarks.Helpers.GetFilePath;
import uk.co.chiragtailor.famouslandmarks.Helpers.Utils;
import uk.co.chiragtailor.famouslandmarks.Models.LocationModel;

public class AddLocation extends AppCompatActivity {
    //initialising
    DatabaseHelper dbHelper;
    private ImageView imageOnAdd;
    private EditText locationName, nearestAirport, countryCode, builtBy, builtYear,latitude, longitude, url, desc;
    private EditText longDesc;
    private String base64Image = null;
    DatabaseHelper db = new DatabaseHelper(AddLocation.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location);
        //set up dbHelper
        dbHelper = new DatabaseHelper(AddLocation.this);
        //calling init function to initialise the fields
        init();
    }

    private void init() {
        imageOnAdd = (ImageView) findViewById(R.id.imageViewOnAddLocation);
        locationName = (EditText) findViewById(R.id.editTextLocationName);
        nearestAirport = (EditText) findViewById(R.id.editTextAirportName);
        countryCode = (EditText) findViewById(R.id.editTextCountryCode);
        builtBy = (EditText) findViewById(R.id.editTextBuiltBy);
        builtYear = (EditText) findViewById(R.id.editTextBuiltYear);
        latitude = (EditText) findViewById(R.id.editTextLatitude);
        longitude = (EditText) findViewById(R.id.editTextLongitude);
        url = (EditText) findViewById(R.id.editTextUrl);
        desc = (EditText) findViewById(R.id.editTextShortDesc);
        longDesc = (EditText) findViewById(R.id.editTextLongDesc);
    }

    @Override
    protected void onStart() {
        //overriding databaseHelpers onStart function to open database
        super.onStart();
        dbHelper.openDB();
    }

    @Override
    protected void onStop() {
        //overriding databaseHelpers onStop function to close database
        super.onStop();
        dbHelper.closeDB();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //initialising MenuInflater
        MenuInflater inflater = getMenuInflater();
        //calling the MenuInflater's inflate function, we pass the main_menu.xml
        inflater.inflate(R.menu.main_menu, menu);
        //removing extra menu option as its not need on this activity screen.
        menu.removeItem(R.id.refreshList);
        menu.removeItem(R.id.addLocation);
        menu.removeItem(R.id.internetDetails);
        menu.removeItem(R.id.mapDetails);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //switching on which button was clicked
        switch (item.getItemId()) {
            //case home is for back button so the user can go back to main screen
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            //next two cases calls alert dialog box
            case R.id.help:
                AlertDialogBuilder.alertDialogShow(AddLocation.this, "help");
                break;
            case R.id.about:
                AlertDialogBuilder.alertDialogShow(AddLocation.this, "about");
                break;
            //saving details to database
            case R.id.saveLocation:
                saveLocationToDB();
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveLocationToDB() {
        //initialising new locationModel
        LocationModel locationModel = new LocationModel();

        //Check that you have select image from gallery or not
        if (base64Image != null){
            locationModel.setImage(base64Image);
        }
        //setting up locationModels setters with values entered in edit text
        locationModel.setLocationName(getValue(locationName));
        locationModel.setNearestAirport(getValue(nearestAirport));
        locationModel.setBuildBy(getValue(builtBy));
        locationModel.setCountryCode(getValue(countryCode));
        locationModel.setBuildYear(getValue(builtYear));
        locationModel.setLatitude(Double.valueOf(getValue(latitude)));
        locationModel.setLongitude(Double.valueOf(getValue(longitude)));
        locationModel.setUrl(getValue(url));
        locationModel.setDesc(getValue(desc));
        locationModel.setLongDesc(getValue(longDesc));
        locationModel.setUserUpload(true);
        //calls db.insert method on databaseHelper passing the whole locationModel
        boolean inserted = db.insert(locationModel);
        //if insert returns true
        if (inserted) {
            // Insert data into database and redirect to list screen
            Toast.makeText(AddLocation.this, "Data inserted successfully", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(AddLocation.this, ListActivity.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(AddLocation.this, "Some error occurred while inserting", Toast.LENGTH_SHORT).show();
        }
    }

    public void imageChooseButtonClick(View view) {
        //if statement to check if the user has given permission for camera, if not than ask for the permission
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else {
            //if permission is given call the function getPhoto
            getPhoto();
        }
    }

    private String getValue(EditText editText) {
        //getValue function is a helper function to get the string of value entered in edit text field
        return editText.getText().toString().trim();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //function called after image is choosen
        super.onActivityResult(requestCode, resultCode, data);
        //check if request code is 1 and result cod is ok and data is not null
        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            //get the path of the image
            Uri selectedImage = data.getData();
            String imgpath = GetFilePath.getPath(AddLocation.this, selectedImage);
            try {
                //make the image in smaller size so it can be stored on database
                String path = Utils.getSmallImageResizeImagePath(AddLocation.this, imgpath, "temporary");
                //get the bitmap
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                imageOnAdd.setImageBitmap(bitmap);

                // Convert image into base64
                InputStream inputStream = getContentResolver().openInputStream(Uri.fromFile(new File(path)));
                ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
                int bufferSize = 1024;
                byte[] buffer = new byte[bufferSize];
                int len = 0;
                while ((len = inputStream.read(buffer)) != -1) {
                    byteBuffer.write(buffer, 0, len);
                }
                byte[] bytes = byteBuffer.toByteArray();
                base64Image = Base64.encodeToString(bytes, Base64.DEFAULT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //function for request permission result to check permission has been given to get photo from media store.
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getPhoto();
            }
        }
    }

    public void getPhoto() {
        //start the activity to get the photo
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 1);
    }
}