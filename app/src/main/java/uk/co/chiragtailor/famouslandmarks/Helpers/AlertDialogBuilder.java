package uk.co.chiragtailor.famouslandmarks.Helpers;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by Chirag on 15/01/2017.
 * this is a helper class to show alert dialog
 */

public class AlertDialogBuilder extends Activity {

    public static void alertDialogShow(Context context, String message)
    {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        switch (message) {
            case "help":
                alertDialog.setTitle("Help");
                alertDialog.setIcon(android.R.drawable.ic_menu_help);
                alertDialog.setMessage("Please click the enter button to start looking through application");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
                break;
            case "about":
                alertDialog.setTitle("About");
                alertDialog.setIcon(android.R.drawable.ic_dialog_info);
                alertDialog.setMessage("This Application shows some of the world's famous landmarks. This application is created by Chirag Tailor (T022515G)");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
                break;
            case "updateLocationOnCreate":
                alertDialog.setTitle("TEST");
                alertDialog.setIcon(android.R.drawable.ic_dialog_info);
                alertDialog.setMessage("This is a test message");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
                break;
            default:
                break;
        }

        alertDialog.show();
    }

}
