package uk.co.chiragtailor.famouslandmarks;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import uk.co.chiragtailor.famouslandmarks.Helpers.AlertDialogBuilder;
import uk.co.chiragtailor.famouslandmarks.Helpers.DatabaseHelper;
import uk.co.chiragtailor.famouslandmarks.Helpers.Utils;
import uk.co.chiragtailor.famouslandmarks.Models.LocationModel;


public class ListActivity extends AppCompatActivity {
    //initialising variables
    private ArrayList<String> locationName = new ArrayList<>();
    private DatabaseHelper dbHelper;
    private ListView locationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        // initialising databaseHelper
        dbHelper = new DatabaseHelper(ListActivity.this);

        //getting action bar (menu) and seeing the home/back button
        try {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

        //Universal Image loader api to load image url from cloud.
        //Allows caching so images will load faster after initial start
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);
        //initialising listView to locationList variable
        locationList = (ListView) findViewById(R.id.locationsListView);
        //initialising ArrayAdapter
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, locationName);
        //setting up the arrayAdapter to locationList
        locationList.setAdapter(arrayAdapter);

        //calls DownloadTask Method to execute url to fetch json data also checks for internet connection
        try {
            if (Utils.isNetworkAvailable(ListActivity.this, true, false)) {
                DownloadTask task = new DownloadTask();
                task.execute("http://api.rain-cloud.co.uk/v1/landmarks");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //options to set up menus
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //initialising MenuInflater
        MenuInflater inflater = getMenuInflater();
        //calling the MenuInflater's inflate function, we pass the main_menu.xml
        inflater.inflate(R.menu.main_menu, menu);
        //removing extra menu option as its not need on this activity screen.
        menu.removeItem(R.id.internetDetails);
        menu.removeItem(R.id.mapDetails);
        menu.removeItem(R.id.saveLocation);
        menu.removeItem(R.id.editLocation);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //switching on which button was clicked
        switch (item.getItemId()) {
            //case home is for back button so the user can go back to main screen
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.refreshList:
                //calls DownloadTask Method to execute url to fetch json data also checks for internet connection
                try {
                    if (Utils.isNetworkAvailable(ListActivity.this, true, false)) {
                        new DownloadTask().execute("http://api.rain-cloud.co.uk/v1/landmarks");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
            //next two cases calls alert dialog box
            case R.id.help:
                AlertDialogBuilder.alertDialogShow(ListActivity.this, "help");
                break;
            case R.id.about:
                AlertDialogBuilder.alertDialogShow(ListActivity.this, "about");
                break;
            case R.id.addLocation:
                //start a new activity. in this case addLocation goes to AddLocation Activity
                Intent intent = new Intent(ListActivity.this, AddLocation.class);
                startActivity(intent);
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public class DownloadTask extends AsyncTask<String, String, List<LocationModel>> {

        @Override
        protected void onPreExecute() {
            //Shows progress dialog
            super.onPreExecute();
            Utils.ShowProgressDialog(ListActivity.this, "Please wait...");
        }

        @Override
        protected List<LocationModel> doInBackground(String... strings) {
            //initialising variable
            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                //storing the url in url variable
                url = new URL(strings[0]);
                //opening url connection
                urlConnection = (HttpURLConnection) url.openConnection();
                //next few lines are initialising inputStream and inputStreamReader
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);
                //starting of reading
                int data = reader.read();
                while (data != -1) {
                    char current = (char) data;
                    result += current;
                    data = reader.read();
                }
                //storing result in finalJSON
                String finalJson = result;
                //Converting JSONObject into JSONArray
                JSONObject parentObject = new JSONObject(finalJson);
                JSONArray parentArray = parentObject.getJSONArray("data");
                //new array list
                List<LocationModel> locationModelList = new ArrayList<>();
                //Google's libraries Gson for easier approach to JSON conversion to arrayList
                Gson gson = new Gson();
                for (int i = 0; i < parentArray.length(); i++) {
                    JSONObject finalObject = parentArray.getJSONObject(i);
                    LocationModel locationModel = gson.fromJson(finalObject.toString(), LocationModel.class);
                    locationModelList.add(locationModel);
                }
                //New arrayList for data coming from SQLite
                List<LocationModel> locationModelList1 = dbHelper.getAllLandmark();
                if (locationModelList1 != null && locationModelList1.size() > 0) {
                    for (int i = 0; i < locationModelList1.size(); i++) {
                        locationModelList.add(locationModelList1.get(i));
                    }
                }
                //returning the arrayList
                return locationModelList;
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(final List<LocationModel> result) {

            super.onPostExecute(result);
            //Once the background process is completed hides the progress dialog
            Utils.hideProgressDialog();
            if (result == null) {
                //if result/data is null than show a toast
                Toast.makeText(getApplicationContext(), "Not able to fetch data from server.", Toast.LENGTH_SHORT).show();
            } else {
                //set up adapter to user row.xml format
                LocationAdapter adapter = new LocationAdapter(getApplicationContext(), R.layout.row, result);
                //set the locationList (listview) to use adapter initialised above
                locationList.setAdapter(adapter);
                //Set up onClickListner
                locationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        //get the position of the row. So which row was clicked
                        LocationModel locationModel = result.get(position);
                        //initialise intent, this click event takes the user to locationDetails Activity
                        Intent intent = new Intent(ListActivity.this, LocationDetails.class);
                        //passing extra value. in this case passing of locationModel
                        intent.putExtra("locationModel", new Gson().toJson(locationModel));
                        //start activity
                        startActivity(intent);
                    }
                });
            }
        }

        public class LocationAdapter extends ArrayAdapter {
            //this is for customer ArrayAdapter for custom listView row.
            private List<LocationModel> locationModelList;
            private int resource;
            private LayoutInflater inflater;

            public LocationAdapter(Context context, int resource, List<LocationModel> objects) {
                super(context, resource, objects);
                locationModelList = objects;
                this.resource = resource;
            }

            @NonNull
            @Override
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                //this function displays data at specified position in Row.xml
                viewHolder holder;
                inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                if (convertView == null) {
                    holder = new viewHolder();
                    convertView = inflater.inflate(resource, parent, false);
                    holder.imageViewOnList = (ImageView) convertView.findViewById(R.id.imageViewOnList);
                    holder.textViewLocationName = (TextView) convertView.findViewById(R.id.textViewLocationName);
                    holder.textViewBuiltYear = (TextView) convertView.findViewById(R.id.textViewBuiltYear);
                    holder.textViewCountryCode = (TextView) convertView.findViewById(R.id.textViewCountryCode);
                    holder.textViewNearestAirport = (TextView) convertView.findViewById(R.id.textViewNearestAirport);
                    holder.textDesc = (TextView) convertView.findViewById(R.id.textDesc);
                    convertView.setTag(holder);
                } else {
                    holder = (viewHolder) convertView.getTag();
                }
                //find progressbar
                final ProgressBar progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);

                //Check that data is uploaded by user or not
                if (locationModelList.get(position).getUserUpload()) {
                    progressBar.setVisibility(View.GONE);
                    byte[] decodedString = Base64.decode(locationModelList.get(position).getImage(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    holder.imageViewOnList.setImageBitmap(decodedByte);
                } else {
                    //Image loading listeners
                    ImageLoader.getInstance().displayImage(locationModelList.get(position).getImage(), holder.imageViewOnList, new ImageLoadingListener() {
                        @Override
                        //on loading event progressbar is shown
                        public void onLoadingStarted(String imageUri, View view) {
                            progressBar.setVisibility(View.VISIBLE);
                        }

                        @Override
                        //on loading failed event progressbar is hidden
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        //on loading complete event progressbar is hidden
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        //on loading cancel progressbar is hidden
                        public void onLoadingCancelled(String imageUri, View view) {
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                }

                //Set the edit text
                holder.textViewLocationName.setText(locationModelList.get(position).getLocationName());
                holder.textViewLocationName.setPaintFlags(holder.textViewLocationName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                holder.textViewBuiltYear.setText("Year Built: " + locationModelList.get(position).getBuildYear());
                holder.textViewCountryCode.setText("Country: " + locationModelList.get(position).getCountryCode());
                holder.textViewNearestAirport.setText("Airport: " + locationModelList.get(position).getNearestAirport());
                holder.textDesc.setText(locationModelList.get(position).getDesc());

                return convertView;
            }

            //using view holder pattern as list view was getting slow as it had to do too much.
            //e.g. find by id multiple times
            class viewHolder {
                private ImageView imageViewOnList;
                private TextView textViewLocationName, textViewBuiltYear, textViewCountryCode, textViewNearestAirport, textDesc;
            }
        }
    }
}


