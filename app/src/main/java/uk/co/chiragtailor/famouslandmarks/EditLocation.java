package uk.co.chiragtailor.famouslandmarks;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import uk.co.chiragtailor.famouslandmarks.Helpers.AlertDialogBuilder;
import uk.co.chiragtailor.famouslandmarks.Helpers.DatabaseHelper;
import uk.co.chiragtailor.famouslandmarks.Helpers.GetFilePath;
import uk.co.chiragtailor.famouslandmarks.Helpers.Utils;
import uk.co.chiragtailor.famouslandmarks.Models.LocationModel;

public class EditLocation extends AppCompatActivity {
    //initialising fields
    private DatabaseHelper dbHelper;
    private ImageView imageOnAdd;
    private EditText locationName, nearestAirport, countryCode, builtBy, builtYear, latitude, longitude, url, desc;
    private EditText longDesc;
    private String base64Image = null;
    private DatabaseHelper db = new DatabaseHelper(EditLocation.this);
    private String id="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_location);
        //setting up dbHelper
        dbHelper = new DatabaseHelper(EditLocation.this);
        //initialising fields
        init();
    }

    private void init() {
        //creating a local instance of each fields
        imageOnAdd = (ImageView) findViewById(R.id.imageViewOnAddLocation);
        locationName = (EditText) findViewById(R.id.editTextLocationName);
        nearestAirport = (EditText) findViewById(R.id.editTextAirportName);
        countryCode = (EditText) findViewById(R.id.editTextCountryCode);
        builtBy = (EditText) findViewById(R.id.editTextBuiltBy);
        builtYear = (EditText) findViewById(R.id.editTextBuiltYear);
        latitude = (EditText) findViewById(R.id.editTextLatitude);
        longitude = (EditText) findViewById(R.id.editTextLongitude);
        url = (EditText) findViewById(R.id.editTextUrl);
        desc = (EditText) findViewById(R.id.editTextShortDesc);
        longDesc = (EditText) findViewById(R.id.editTextLongDesc);
        // getting extra in bundle passed from parent activity. in this case it will be locationModel
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) { //checing if bundle is not null
            String json = bundle.getString("locationModel"); //storing the string in json variable
            LocationModel locationModel = new Gson().fromJson(json, LocationModel.class); // using GSON to store locationModel
            id = locationModel.get_id(); //getting the id from locationModel
            //Code to check user upload data for images or if the data uploaded is sample data
            if(locationModel.getImage() != null) //checking image is not null
            {
                base64Image = locationModel.getImage(); //storing in base64Image variable
                //decoding the base64image
                byte[] decodedString = Base64.decode(locationModel.getImage(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                imageOnAdd.setImageBitmap(decodedByte);
            }

            //setting up editField using data in locationModel
            locationName.setText(locationModel.getLocationName());
            nearestAirport.setText(locationModel.getNearestAirport());
            countryCode.setText(locationModel.getCountryCode());
            builtBy.setText(locationModel.getBuildBy());
            builtYear.setText(locationModel.getBuildYear());
            latitude.setText(""+ locationModel.getLatitude());
            longitude.setText(""+ locationModel.getLongitude());
            url.setText(locationModel.getUrl());
            desc.setText(locationModel.getDesc());
            longDesc.setText(locationModel.getLongDesc());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //overriding databaseHelpers onStart function to open database
        dbHelper.openDB();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //overriding databaseHelpers onStop function to close database
        dbHelper.closeDB();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //initialising MenuInflater
        MenuInflater inflater = getMenuInflater();
        //calling the MenuInflater's inflate function, we pass the main_menu.xml
        inflater.inflate(R.menu.main_menu, menu);
        //removing extra menu option as its not need on this activity screen.
        menu.removeItem(R.id.refreshList);
        menu.removeItem(R.id.addLocation);
        menu.removeItem(R.id.internetDetails);
        menu.removeItem(R.id.mapDetails);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //switching on which button was clicked
        switch (item.getItemId()) {
            case android.R.id.home:
                //case home is for back button so the user can go back to main screen
                NavUtils.navigateUpFromSameTask(this);
                return true;
            //next two cases calls alert dialog box
            case R.id.help:
                AlertDialogBuilder.alertDialogShow(EditLocation.this, "help");
                break;
            case R.id.about:
                AlertDialogBuilder.alertDialogShow(EditLocation.this, "about");
                break;
            //saving details to database
            case R.id.saveLocation:
                updateLocationToDB();
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateLocationToDB() {
        //setting up new locationModel
        LocationModel locationModel = new LocationModel();

        //Check that you have select image from gallery or not
        if (base64Image != null) {
            locationModel.setImage(base64Image);
        }
        //set rest of the fields in locationModel
        locationModel.set_id(id);
        locationModel.setLocationName(getValue(locationName));
        locationModel.setNearestAirport(getValue(nearestAirport));
        locationModel.setBuildBy(getValue(builtBy));
        locationModel.setCountryCode(getValue(countryCode));
        locationModel.setBuildYear(getValue(builtYear));
        locationModel.setLatitude(Double.valueOf(getValue(latitude)));
        locationModel.setLongitude(Double.valueOf(getValue(longitude)));
        locationModel.setUrl(getValue(url));
        locationModel.setDesc(getValue(desc));
        locationModel.setLongDesc(getValue(longDesc));
        locationModel.setUserUpload(true);

        //call update function on databaseHelper
        boolean updated = db.update(locationModel, id);
        if (updated) {
            //if true than does the following
            // Insert data into database and redirect to list screen
            Toast.makeText(EditLocation.this, "Data updated successfully", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(EditLocation.this, ListActivity.class);
            startActivity(intent);
            LocationDetails.activity.finish();
            finish();
        } else {
            //show toast with error message
            Toast.makeText(EditLocation.this, "Some error occurred while inserting", Toast.LENGTH_SHORT).show();
        }
    }

    public void imageChooseButtonClick(View view) {
        //if statement to check if the user has given permission for camera, if not than ask for the permission
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else {
            //if permission is given call the function getPhoto
            getPhoto();
        }
    }

    private String getValue(EditText editText) {
        return editText.getText().toString().trim();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //function called after image is choosen
        super.onActivityResult(requestCode, resultCode, data);
        //check if request code is 1 and result cod is ok and data is not null
        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            //get the path of the image
            Uri selectedImage = data.getData();
            String imgpath = GetFilePath.getPath(EditLocation.this, selectedImage);
            try {
                //make the image in smaller size so it can be stored on database
                String path = Utils.getSmallImageResizeImagePath(EditLocation.this, imgpath, "temporary");
                //get the bitmap
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                imageOnAdd.setImageBitmap(bitmap);

                // Convert image into base64
                InputStream inputStream = getContentResolver().openInputStream(Uri.fromFile(new File(path)));
                ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
                int bufferSize = 1024;
                byte[] buffer = new byte[bufferSize];
                int len = 0;
                while ((len = inputStream.read(buffer)) != -1) {
                    byteBuffer.write(buffer, 0, len);
                }
                byte[] bytes = byteBuffer.toByteArray();
                base64Image = Base64.encodeToString(bytes, Base64.DEFAULT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //function for request permission result to check permission has been given to get photo from media store.
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getPhoto();
            }
        }
    }

    public void getPhoto() {
        //start the activity to get the photo
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 1);
    }
}