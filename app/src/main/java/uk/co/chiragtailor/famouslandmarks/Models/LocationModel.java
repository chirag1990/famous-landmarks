package uk.co.chiragtailor.famouslandmarks.Models;

/**
 * Created by Chirag on 12/01/2017.
 * in this file we set up an model for locations.
 */

public class LocationModel {
    private String _id;
    private String createdAt;
    private String updatedAt;
    private String nearestAirport;
    private String image;
    private Double longitude;
    private Double latitude;
    private String url;
    private String buildBy;
    private String buildYear;
    private String countryCode;
    private String locationName;
    private String desc;
    private String longDesc;
    private Boolean userUpload;
    private int buildYearSQL;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getNearestAirport() {
        return nearestAirport;
    }

    public void setNearestAirport(String nearestAirport) {
        this.nearestAirport = nearestAirport;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBuildBy() {
        return buildBy;
    }

    public void setBuildBy(String buildBy) {
        this.buildBy = buildBy;
    }

    public String getBuildYear() {
        return buildYear;
    }

    public void setBuildYear(String buildYear) {
        this.buildYear = buildYear;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLongDesc() {
        return longDesc;
    }

    public void setLongDesc(String longDesc) {
        this.longDesc = longDesc;
    }

    public Boolean getUserUpload() {
        return userUpload;
    }

    public void setUserUpload(Boolean userUpload) {
        this.userUpload = userUpload;
    }

    public int getBuildYearSQL() {
        return buildYearSQL;
    }

    public void setBuildYearSQL(int buildYearSQL) {
        this.buildYearSQL = buildYearSQL;
    }


}
