package uk.co.chiragtailor.famouslandmarks.Helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import uk.co.chiragtailor.famouslandmarks.Models.LocationModel;

/**
 * Created by Chirag on 15/01/2017.
 * this is a databasehelper class to set up database, create tables, insert data, update data, delete data
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "famousLandmarks.db";
    private static final int DATABASE_VERSION = 1;
    private SQLiteDatabase database;
    private String queryTable = "CREATE TABLE " + TABLE_NAME + " (\n" +
            ID + " INTEGER PRIMARY KEY, AUTO INCREMENT,\n" +
            CREATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP, \n" +
            UPDATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP, \n" +
            LOCATION_NAME + " TEXT, \n" +
            IMAGE + " TEXT, \n" +
            COUNTRY_CODE + " TEXT, \n" +
            BUILD_BY + " TEXT, \n" +
            NEAREST_AIRPORT + " TEXT, \n" +
            BUILD_YEAR + " TEXT, \n" +
            LATITUDE + " REAL, \n" +
            LONGITUDE + " REAL, \n" +
            URL + " TEXT, \n" +
            SHORT_DESC + " TEXT, \n" +
            LONG_DESC + " TEXT, \n" +
            USER_UPLOAD + " INTEGER \n" +
            ")";
    private static final String TABLE_NAME = "landmarks";
    public static final String ID = "ID";
    public static final String CREATED_AT = "createAt";
    public static final String UPDATED_AT = "updatedAt";
    public static final String IMAGE = "image";
    public static final String LOCATION_NAME = "locationName";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String BUILD_BY = "builtBy";
    public static final String NEAREST_AIRPORT = "nearestAirport";
    public static final String BUILD_YEAR = "buildYear";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String URL = "url";
    public static final String SHORT_DESC = "desc";
    public static final String LONG_DESC = "longDesc";
    public static final String USER_UPLOAD = "userUpload";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("MyDBHandler", "onCreate: Creating a new database structure");
        db.execSQL(queryTable);
        Log.i("queryTable", queryTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS landmarks ");
        onCreate(db);
    }

    public void openDB() {
        database = getWritableDatabase();
    }

    public void closeDB() {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }


    // get all landmark list from database
    public ArrayList<LocationModel> getAllLandmark() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM landmarks", null);
        ArrayList<LocationModel> all_location = new ArrayList<>();
        LocationModel locationModel;

        Log.i("IN GETLANDMARK", "IN GETLANDMARK");

        if (cursor.moveToFirst()) {

            do {
                locationModel = new LocationModel();
                locationModel.set_id(cursor.getString(cursor.getColumnIndex(ID)));
                locationModel.setLocationName(cursor.getString(cursor.getColumnIndex(LOCATION_NAME)));
                locationModel.setImage(cursor.getString(cursor.getColumnIndex(IMAGE)));
                locationModel.setBuildBy(cursor.getString(cursor.getColumnIndex(BUILD_BY)));
                locationModel.setCountryCode(cursor.getString(cursor.getColumnIndex(COUNTRY_CODE)));
                locationModel.setNearestAirport(cursor.getString(cursor.getColumnIndex(NEAREST_AIRPORT)));
                locationModel.setBuildYear(cursor.getString(cursor.getColumnIndex(BUILD_YEAR)));
                locationModel.setLatitude(cursor.getDouble(cursor.getColumnIndex(LATITUDE)));
                locationModel.setLongitude(cursor.getDouble(cursor.getColumnIndex(LONGITUDE)));
                locationModel.setUrl(cursor.getString(cursor.getColumnIndex(URL)));
                locationModel.setDesc(cursor.getString(cursor.getColumnIndex(SHORT_DESC)));
                locationModel.setLongDesc(cursor.getString(cursor.getColumnIndex(LONG_DESC)));
                locationModel.setUserUpload(cursor.getInt(cursor.getColumnIndex(USER_UPLOAD)) > 0);
                all_location.add(locationModel);

            } while (cursor.moveToNext());
        }

        Log.i("OUT GETLANDMARK", "OUT GETLANDMARK");
        return all_location;
    }

    // Insert landmark in database
    public boolean insert(LocationModel locationModel) {

        Log.i("IN INSERT", "IN INSERT");
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ID, locationModel.get_id());
        values.put(LOCATION_NAME, locationModel.getLocationName());
        values.put(IMAGE, locationModel.getImage());
        values.put(BUILD_BY, locationModel.getBuildBy());
        values.put(COUNTRY_CODE, locationModel.getCountryCode());
        values.put(NEAREST_AIRPORT, locationModel.getNearestAirport());
        values.put(BUILD_YEAR, locationModel.getBuildYear());
        values.put(LATITUDE, locationModel.getLatitude());
        values.put(LONGITUDE, locationModel.getLongitude());
        values.put(URL, locationModel.getUrl());
        values.put(SHORT_DESC, locationModel.getDesc());
        values.put(LONG_DESC, locationModel.getLongDesc());
        values.put(USER_UPLOAD, locationModel.getUserUpload());

        long inserted = db.insert("landmarks", null, values);
        Log.i("OUT INSERT", "OUT INSERT");
        if (inserted != -1) {
            return true;
        } else {
            return false;
        }
    }

    public boolean update(LocationModel locationModel, String Id) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(ID, locationModel.get_id());
            values.put(IMAGE, locationModel.getImage());
            values.put(LOCATION_NAME, locationModel.getLocationName());
            values.put(BUILD_BY, locationModel.getBuildBy());
            values.put(COUNTRY_CODE, locationModel.getCountryCode());
            values.put(NEAREST_AIRPORT, locationModel.getNearestAirport());
            values.put(BUILD_YEAR, locationModel.getBuildYear());
            values.put(LATITUDE, locationModel.getLatitude());
            values.put(LONGITUDE, locationModel.getLongitude());
            values.put(URL, locationModel.getUrl());
            values.put(SHORT_DESC, locationModel.getDesc());
            values.put(LONG_DESC, locationModel.getLongDesc());
            values.put(USER_UPLOAD, locationModel.getUserUpload());
            db.update(TABLE_NAME, values, ID + " = ?", new String[]{Id});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            SQLiteDatabase.releaseMemory();
        }
        return false;
    }


    // Delete one data from database
    public void delete(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String where = ID + " = " + id;
        String query = "Delete From " + TABLE_NAME + " where " + where;
        db.execSQL(query);
    }


    public void dropTable() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS landmarks");
    }

    public void createTable() {
        Log.e("", "query ::" + queryTable);
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(queryTable);
    }
}