package uk.co.chiragtailor.famouslandmarks;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.gson.Gson;

import uk.co.chiragtailor.famouslandmarks.Helpers.AlertDialogBuilder;
import uk.co.chiragtailor.famouslandmarks.Models.LocationModel;

public class webView extends AppCompatActivity {

    private LocationModel locationModel;
    private String webUrl;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        //getting action bar (menu) and seeing the home/back button
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        try {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }catch (NullPointerException ex){
            ex.printStackTrace();
        }

        //initialise bundle and check if bundle is not null, set up locationModel
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            String json = bundle.getString("locationModel");
            locationModel = new Gson().fromJson(json, LocationModel.class);
        }

        //get the url from locationModel and store it in webUrl
        webUrl = locationModel.getUrl();

        //initialise the webView and load the url
        webView = (WebView) findViewById(R.id.locationWebView);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl(webUrl);

        //force other links to open in webView rather than other browser
        webView.setWebViewClient(new webViewClient());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //set up menu and remove the buttons which are not required in this view
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        menu.removeItem(R.id.refreshList);
        menu.removeItem(R.id.internetDetails);
        menu.removeItem(R.id.addLocation);
        menu.removeItem(R.id.saveLocation);
        menu.removeItem(R.id.editLocation);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                //when back button is clicked it should take the user back to locationDetails
                Intent intent = new Intent(webView.this,LocationDetails.class);
                intent.putExtra("locationModel", new Gson().toJson(locationModel));
                startActivity(intent);
                return true;
            case R.id.mapDetails:
                //start an activity to mapview and pass locationModel
                Intent mapIntent = new Intent(webView.this, MapsActivity.class);
                mapIntent.putExtra("locationModel", new Gson().toJson(locationModel));
                startActivity(mapIntent);
                break;
            //next two case calls alert dialogs
            case R.id.help:
                AlertDialogBuilder.alertDialogShow(webView.this,"help");
                break;
            case R.id.about:
                AlertDialogBuilder.alertDialogShow(webView.this,"about");
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    private class webViewClient extends WebViewClient {
        @Override
        //allows the user to open other links in application rather than opening in browser.
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            String requestUrl = String.valueOf(request.getUrl());

            if (Uri.parse(requestUrl).equals(webUrl)) {
                return false;
            }else {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(requestUrl));
                startActivity(intent);
                return true;
            }
        }
    }

    //goto previous page when pressing back button

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (webView.canGoBack()) {
                        webView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}
