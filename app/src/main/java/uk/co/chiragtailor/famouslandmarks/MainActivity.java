package uk.co.chiragtailor.famouslandmarks;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;

import uk.co.chiragtailor.famouslandmarks.Helpers.AlertDialogBuilder;
import uk.co.chiragtailor.famouslandmarks.Helpers.DatabaseHelper;
import uk.co.chiragtailor.famouslandmarks.Models.LocationModel;

public class MainActivity extends AppCompatActivity {
    private DatabaseHelper dbHelper;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        //initialising database helper so its functions can be called with dbHelper.
        dbHelper = new DatabaseHelper(MainActivity.this);

        //used for downloading image from network and checking that network has thrown error
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //Find textviews and starts the animate
        TextView textViewLandMark = (TextView) findViewById(R.id.mainActivityTextView);
        textViewLandMark.animate().alpha(1f).setDuration(100);
        //Finds imageview and starting the animate
        ImageView imageLandMark = (ImageView) findViewById(R.id.imageView);
        imageLandMark.animate().alpha(1f).setDuration(3500);
        //Next four lines finds the button and starts the animate
        Button enterButton = (Button) findViewById(R.id.btnEnter);
        enterButton.animate().alpha(1f).setDuration(8000);

        Button btnDropTable = (Button) findViewById(R.id.btnDropTable);
        btnDropTable.animate().alpha(1f).setDuration(8000);

        //Setting up onClickListener for btnDropTable and calls DownloadFileAsync method
        btnDropTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DownloadFileAsync().execute("");
            }
        });

    }

    private class DownloadFileAsync extends AsyncTask<String, Void, Boolean> {
        //onPreExecute is run before starting the function.
        //onPreExecute user is shown a dialog for please wait..
        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(MainActivity.this, "", "Please wait...");
        }

        @Override
        protected Boolean doInBackground(String... params) {
            /*Boolean success;
            return success = false;*/
            return false;
        }
        //onPostExecute we try and run queries in databasehelper to droptable, createtable and insert initial data
        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            try {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        dbHelper.dropTable();
                        dbHelper.createTable();
                        saveLocationToDB();
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();
                    }
                }, 1500);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    // Sample data
    private void saveLocationToDB() {
        try {
            //String base64Image = null;
            String base64Image;
            LocationModel locationModel = new LocationModel();
            URL url = new URL("http://www.visitdubaishoppingfestival.com/wp-content/uploads/2015/04/burj-khalifa.jpg");
            InputStream in = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            //int n = 0;
            int n;
            while (-1 != (n = in.read(buf))) {
                out.write(buf, 0, n);
            }
            out.close();
            in.close();
            byte[] response = out.toByteArray();
            base64Image = Base64.encodeToString(response, Base64.DEFAULT);

            //Check that you have select image from gallery or not
            if (base64Image != null)
                locationModel.setImage(base64Image);
            locationModel.setLocationName("Burj Khalifa");
            locationModel.setNearestAirport("DBX");
            locationModel.setBuildBy("Adrian Smith");
            locationModel.setCountryCode("DUBAI");
            locationModel.setBuildYear("2009");
            locationModel.setLatitude(25.197525);
            locationModel.setLongitude(55.274288);
            locationModel.setUrl("http://www.burjkhalifa.ae/en/index.aspx");
            locationModel.setDesc("The Burj Khalifa, known as the Burj Dubai before its inauguration, is a megatall skyscraper in Dubai, United Arab Emirates. It is the tallest structure in the world, standing at 829.8 m.");
            locationModel.setLongDesc("About Burj Khalifa Burj Khalifa has redefined what is possible in the design and engineering of supertall buildings. By combining cutting-edge technologies and cultural influences, the building serves as a global icon that is both a model for future urban centers and speaks to the global movement towards compact, livable urban areas. The Tower and its surrounding neighborhood are more centralized than any other new development in Dubai. At the center of a new downtown neighborhood, Burj Khalifa’s mixed-use program focuses the area’s development density and provides direct connections to mass transit systems.\n" +
                    "               Burj Khalifa’s architecture has embodied references to Islamic architecture and yet reflects the modern global community it is designed to serve. The building’s Y-shaped plan provides the maximum amount of perimeter for windows in living spaces without developing internal unusable area. As the tapering tower rises, setbacks occur at the ends of each “wing” in an upward spiraling pattern that decreases the mass of the tower as the height increases. These setbacks were modeled in the wind tunnel to minimize wind forces. The design of the Tower was significantly influenced by its performance with respect to the wind, in both its shaping and orientation. The building went through many wind tunnel tests and design iterations to develop optimum performance.\n" +
                    "        The exterior cladding, comprised of aluminum and textured stainless steel spandrel panels, was designed to withstand Dubai’s extreme temperatures during the summer months by using a low-E glass to provide enhanced thermal insulation. Vertical polished stainless steel fins were added to accentuate Burj Khalifa’s height and slenderness.\n" +
                    "               The unprecedented height of the Burj Khalifa required it to be an innovative building in many ways. Design techniques, building systems, and construction practices all required rethinking, and in many cases new applications, to create a practical and efficient building.\n" +
                    "               The structural system, termed a “buttressed core,” is designed to efficiently support a supertall building utilizing a strong central core, buttressed by its three wings. The vertical structure is tied together at the mechanical floors through outrigger walls in order to maximize the building’s stiffness. The result is an efficient system where all of the building’s vertical structure is used to support both gravity and lateral loads.");
            locationModel.setUserUpload(true);

            boolean inserted = dbHelper.insert(locationModel);

            if (inserted) {
                Toast.makeText(MainActivity.this, "Data process completed successfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "Some error occurred while inserting", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //options to set up menus
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //initialising MenuInflater
        MenuInflater menuInflater = getMenuInflater();
        //calling the MenuInflater's inflate function, we pass the main_menu.xml
        menuInflater.inflate(R.menu.main_menu, menu);
        //removing extra menu option as its not need on this activity screen.
        menu.removeItem(R.id.refreshList);
        menu.removeItem(R.id.internetDetails);
        menu.removeItem(R.id.mapDetails);
        menu.removeItem(R.id.addLocation);
        menu.removeItem(R.id.saveLocation);
        menu.removeItem(R.id.editLocation);
        return super.onCreateOptionsMenu(menu);
    }

    //Items selected on menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        //switching on which button was clicked and depending on the click calls the alert dialog
        switch (item.getItemId()) {
            case R.id.help:
                AlertDialogBuilder.alertDialogShow(MainActivity.this, "help");
                break;
            case R.id.about:
                AlertDialogBuilder.alertDialogShow(MainActivity.this, "about");
                break;

            default:
                break;

        }
        return true;
    }

    //when enter button is clicked
    public void onClickEnter(View view) {
        //start of new activity.
        Intent intent = new Intent(getApplicationContext(), ListActivity.class);
        startActivity(intent);
    }
}
